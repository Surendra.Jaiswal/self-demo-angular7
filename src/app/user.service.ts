import { Injectable } from '@angular/core';
import { User } from './User';
import { USERS } from './mock-Users';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() { }
  
  
  getUsers(): Observable<User[]>{
    console.log("service is called");
    return of (USERS);
  }
}
