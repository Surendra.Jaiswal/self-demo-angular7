import { Component, OnInit,} from '@angular/core';
import { User } from '../User';
import { USERS } from '../mock-Users';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-list-view',
  templateUrl: './user-list-view.component.html',
  styleUrls: ['./user-list-view.component.css']
})
export class UserListViewComponent implements OnInit {
  selectedUser: User;
  public users : User[];
  

  constructor(private userService: UserService) { }

  ngOnInit() { 
    console.log("inside init method");
    this.getUsers();
  }
  getUsers(){
    this.users = this.userService.getUsers();
  }
 
   legend = "StormLordArdan";
 
  onSelect(userSel: User): void {
    console.log("selected user : "+userSel.name);
  this.selectedUser = userSel;
}


}
